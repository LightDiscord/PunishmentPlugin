package fr.gcommunity.tournoi;

import com.google.common.collect.Lists;
import fr.gcommunity.message.BukkitMessageBuilder;
import fr.gcommunity.tournoi.models.Punishment;
import net.md_5.bungee.api.chat.ClickEvent;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lightdiscord on 12/04/17.
 */
public class Commando implements CommandExecutor, TabCompleter{

    public final Map<String, Punishment> registered;

    public Commando(Map<String, Punishment> registered){
        this.registered = registered;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!commandSender.hasPermission("punisher.use")) {
            return true;
        }

        if (args.length < 3) {

            if (args.length == 0) {
                if(!(commandSender instanceof Player)) commandSender.sendMessage(ChatColor.GRAY + "Liste des punissions ici : /punisher list\nPunir quelqu'un (ou enlever sa sentence) avec : /punisher <punission> <utilisateur>");
                else{
                    new BukkitMessageBuilder("§7Liste des punissions ici : ").next("§6/punisher list").click(ClickEvent.Action.RUN_COMMAND, "/punisher list")
                    .setHover("§2Executer la commande.").nextln("§7Punir quelqu'un (ou enlever sa sentence) avec : §6/punisher <punission> <utilisateur>").send((Player)commandSender);
                }
                return true;
            }

            if (args[0].equalsIgnoreCase("list")) {
                if(!(commandSender instanceof Player)) commandSender.sendMessage(ChatColor.GRAY + "Liste des punissions (Tips: Clic pour générer) : " + StringUtils.join(registered.keySet().toArray(), ", "));
                else {
                    BukkitMessageBuilder bmb = new BukkitMessageBuilder("");
                    bmb.next("§7Liste des punissions (Tips: Clic pour générer) : ");
                    int i = 0;
                    for (String name : registered.keySet()) {
                        if (i != 0) bmb.next("§7,§r ");
                        bmb.next(name).click(ClickEvent.Action.SUGGEST_COMMAND, "/punisher " + name).setHover("§2Générer la commande.");
                        i++;
                    }
                    bmb.send((Player)commandSender);
                }
                return true;
            }

            if (args.length < 2) {
                if(!(commandSender instanceof Player)) commandSender.sendMessage(ChatColor.RED + "Il n'y a pas assez d'arguments, fait /punisher pour plus de détails !");
                else{
                    new BukkitMessageBuilder("§cIl n'y a pas assez d'arguments, fait \"").next("§6/punisher").click(ClickEvent.Action.RUN_COMMAND, "/punisher")
                            .setHover("§2Executer la commande.").next("\" §cpour plus de détails !").send((Player)commandSender);
                }
                return true;
            }

            Player target = Bukkit.getPlayerExact(args[1]);

            if (target == null || !target.isOnline()) {
                commandSender.sendMessage(ChatColor.RED + "L'utilisateur renseigné est introuvable ! :c");
                return true;
            }

            Punishment punishment = find(args[0]);

            if (punishment == null) {
                if(!(commandSender instanceof Player)) commandSender.sendMessage(ChatColor.RED + "Tu dois renseigner un nom de punission valide ! Voir \"/punisher list\" pour une liste des punissions!");
                else{
                    new BukkitMessageBuilder("§cTu dois renseigner un nom de punission valide ! Voir \"").next("§6/punisher list").click(ClickEvent.Action.RUN_COMMAND, "/punisher list")
                    .setHover("§2Executer la commande.").next("\" §cpour une liste des punissions!").send((Player)commandSender);
                }
                return true;
            }

            if (punishment.toActive(target)) {
                commandSender.sendMessage(ChatColor.GREEN + "Niquel! " + ChatColor.GRAY + "Tu viens de punir ta cible. Me gusta.");
            } else {
                commandSender.sendMessage(ChatColor.GREEN + "Bien! " + ChatColor.GRAY + "La punission vient d'être retiré.");
            }
            punishment.execute(target);

        } else {
            if(!(commandSender instanceof Player)) commandSender.sendMessage(ChatColor.RED + "Il y a trop d'arguments, fait /punisher pour plus de détails !");
            else{
                new BukkitMessageBuilder("§cIl y a trop d'arguments, fait \"").next("§6/punisher").click(ClickEvent.Action.RUN_COMMAND, "/punisher")
                        .setHover("§2Executer la commande.").next("\" §cpour plus de détails !").send((Player)commandSender);
            }
        }
        return true;
    }

    public Punishment find(String name) {
        return registered.get(name);
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        if(strings.length == 1){
            List<String> args = Lists.newArrayList();
            args.add("list");
            for(String name : registered.keySet()){
                if(name.toLowerCase().startsWith(strings[0].toLowerCase())) args.add(name);
            }
            return args;
        }
        return null;
    }
}
