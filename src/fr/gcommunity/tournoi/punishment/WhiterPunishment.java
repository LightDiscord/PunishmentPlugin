package fr.gcommunity.tournoi.punishment;

import fr.gcommunity.tournoi.models.Punishment;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wither;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by NeutronStars
 */
public class WhiterPunishment extends Punishment{

    public WhiterPunishment(){
        super("whiter", false);
    }

    public void execute(Player player){
        player.getInventory().clear();
        ItemStack is = new ItemStack(Material.WOOD_SWORD, 1);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName("§d§lJustice Sword");
        is.setItemMeta(im);
        player.getInventory().addItem(is);

        player.sendMessage("§8Combat toi pour la §cjustice §8! Mais surtout, §cbats-toi §8si t'es un homme !");
        player.setBedSpawnLocation(player.getLocation(), true);

        for(int i = 0; i < 5; i++) {
            Wither wither = (Wither) player.getWorld().spawnEntity(player.getLocation(), EntityType.WITHER);
            wither.setCustomName("La punission est arrivée !");
        }
    }
}
