package fr.gcommunity.tournoi.punishment;

import fr.gcommunity.tournoi.models.Punishment;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NeutronStars.
 */
public class BlewMiningPunishment extends Punishment implements Listener {

    public List<String> sentences = new ArrayList<String>();

    public BlewMiningPunishment(Plugin plugin){
        super("blew-mining", true);
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public void execute(Player player){
        if (punished.contains(player)) {
            punished.remove(player);
        } else {
            punished.add(player);
        }
    }


    @EventHandler
    public void onDamage(BlockBreakEvent event) {
        Player player = (Player) event.getPlayer();
        if (!punished.contains(player)) return;

        event.setCancelled(true);

        Location location = event.getBlock().getLocation();

        player.getWorld().createExplosion(location.getBlockX(), location.getBlockY(), location.getBlockZ(), 5.0f, true, true);
    }
}
