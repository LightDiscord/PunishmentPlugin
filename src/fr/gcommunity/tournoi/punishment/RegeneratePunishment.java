package fr.gcommunity.tournoi.punishment;

import fr.gcommunity.tournoi.models.Punishment;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by lightdiscord.
 */
public class RegeneratePunishment extends Punishment implements Listener {

    public List<String> sentences = new ArrayList<String>();

    public RegeneratePunishment(Plugin plugin){
        super("regenerate", true);
        Bukkit.getPluginManager().registerEvents(this, plugin);
        sentences.add("*Ouups, visiblement tu ne peux rien casser.*");
        sentences.add("*Mince alors, tu ne peux casser rien truc.*");
        sentences.add("*Dommage pour toi, tu ne peux pas casser ça.*");
    }

    public void execute(Player player){
        if (punished.contains(player)) {
            punished.remove(player);
        } else {
            punished.add(player);
        }
    }


    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if (!punished.contains(player)) return;

        event.setCancelled(true);

        player.sendMessage(new StringBuilder().append(ChatColor.GRAY).append(ChatColor.ITALIC).append(sentences.get(new Random().nextInt(sentences.size()))).toString());
    }
}
