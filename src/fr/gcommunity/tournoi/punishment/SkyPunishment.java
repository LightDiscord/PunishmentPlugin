package fr.gcommunity.tournoi.punishment;

import fr.gcommunity.tournoi.models.Punishment;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.*;

/**
 * Created by lightdiscord on 12/04/17.
 */
public class SkyPunishment extends Punishment {

    public Plugin plugin;

    public void execute(Player player) {
        Location pLoc = player.getLocation();
        Location center = new Location(pLoc.getWorld(), pLoc.getBlockX(), 255, pLoc.getBlockZ());

        for (int x = -2; x < 3; x++)
            for (int z = -2; z < 3; z++)
                player.getLocation().getWorld().getBlockAt(center.getBlockX() + x, center.getBlockY(), center.getBlockZ() + z).setType(Material.GLASS);

        Location teleport = center.clone();
        teleport.setY(257);

        player.teleport(teleport);
        player.sendMessage(new StringBuilder().append(ChatColor.GRAY).append(ChatColor.ITALIC).append("Il fait un peu froid là haut, mais attention à la chute!").toString());

        Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                for (int x = -2; x < 3; x++)
                    for (int z = -2; z < 3; z++)
                        player.getLocation().getWorld().getBlockAt(center.getBlockX() + x, center.getBlockY(), center.getBlockZ() + z).setType(Material.AIR);

                player.sendMessage(new StringBuilder().append(ChatColor.GRAY).append(ChatColor.ITALIC).append("J'avais dit quoi ? Attention à la chute !").toString());
            }
        }, 5 * 20);
    }

    public SkyPunishment(Plugin plugin) {
        super("sky", false);
        this.plugin = plugin;
    }
}
